import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  public folder: string;
  payButton = document.getElementById("pay-button");
  form = document.getElementById("payment-form");
  errorStack = [];
  
  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('id');
    // $('head').append('<script async src="https://cdn.checkout.com/js/framesv2.min.js"></script>');
    // $('head').append('<script async src="assets/JS/app.js"></script>');


    let scriptElement = document.createElement("script");
    scriptElement.type = "text/javascript";
    // let iframeObj = <HTMLIFrameElement>document.getElementById('hyperPayform');
    let hyperOptionsScriptEl = document.createElement("script");
    let hyperOptionsScriptElContent = document.createTextNode(`
    var payButton = document.getElementById("pay-button");
    var form = document.getElementById("payment-form");
    var errorStack = [];
    
    window.Frames.init("pk_test_8ac41c0d-fbcc-4ae3-a771-31ea533a2beb");
    window.Frames.addEventHandler(
      window.Frames.Events.CARD_VALIDATION_CHANGED,
      onCardValidationChanged
    );
    function onCardValidationChanged(event) {
      console.log("CARD_VALIDATION_CHANGED: %o", event);
      payButton.disabled = !window.Frames.isCardValid();
    }
    
    window.Frames.addEventHandler(
      window.Frames.Events.FRAME_VALIDATION_CHANGED,
      onValidationChanged
    );
    function onValidationChanged(event) {
      console.log("FRAME_VALIDATION_CHANGED: %o", event);
    
      var errorMessageElement = document.querySelector(".error-message");
      var hasError = !event.isValid && !event.isEmpty;
    
      if (hasError) {
        errorStack.push(event.element);
      } else {
        errorStack = errorStack.filter(function (element) {
          return element !== event.element;
        });
      }
    
      var errorMessage = errorStack.length
        ? getErrorMessage(errorStack[errorStack.length - 1])
        : "";
      errorMessageElement.textContent = errorMessage;
    }
    
    function getErrorMessage(element) {
      var errors = {
        "card-number": "Please enter a valid card number",
        "expiry-date": "Please enter a valid expiry date",
        cvv: "Please enter a valid cvv code",
      };
    
      return errors[element];
    }
    
    window.Frames.addEventHandler(
      window.Frames.Events.CARD_TOKENIZATION_FAILED,
      onCardTokenizationFailed
    );
    function onCardTokenizationFailed(error) {
      console.log("CARD_TOKENIZATION_FAILED: %o", error);
      window.Frames.enableSubmitForm();
    }
    
    window.Frames.addEventHandler(window.Frames.Events.CARD_TOKENIZED, onCardTokenized);
    function onCardTokenized(event) {
      var el = document.querySelector(".success-payment-message");
      el.innerHTML =
        "Card tokenization completed<br>" +
        'Your card token is: <span class="token">' +
        event.token +
        "</span>";
    }
    
    form.addEventListener("submit", function (event) {
      event.preventDefault();
      window.Frames.submitCard();
    });`);

      hyperOptionsScriptEl.appendChild(hyperOptionsScriptElContent);
      document.getElementById('wrapper').appendChild(hyperOptionsScriptEl);
  }
}
